# Import required modules
import random

# Set a Dice
Dice = [1, 2, 3, 4, 5, 6]

print("LET'S START THE GAME\n")

# Game Function
def Rolling():
    """
    Start Craps Games
    """

    # First Roll Result
    Layout = random.choice(Dice) + random.choice(Dice)
    print("Your Layout: ",Layout)

    # If Result 7/11 = Win
    if (Layout == 7 or Layout == 11):
        print("You Win :)")

    # If Result 2/3/12 = Lose
    elif (Layout == 2 or Layout == 3 or Layout == 12):
        print("You Lose :(")

    # If Result 4/5/6/8/9/10 = Reroll
    elif (Layout == 4 or Layout == 5 or Layout == 6 or Layout == 8 or Layout == 9 or Layout == 10):
        print("Point, Roll Again!!")
        Layout2 = random.choice(Dice) + random.choice(Dice)
        print("Your Second Layout: ",Layout2)

        x = 0
        while x == 0:

            # If Reroll = First Roll = Win
            if Layout2 == Layout:
                print("You Win :)")
                break

            # If Reroll = 7 = Lose
            elif Layout2 == 7:
                print("You Lose :(")
                break

            # Reroll until Reroll = First Roll or 7
            else:
                print("Point, Roll Again!!")
                Layout2 = random.choice(Dice) + random.choice(Dice)
                print("Your Point Layout: ",Layout2)
                x = 0


Rolling()